import help from './help.md'

export const help = help
export const title = 'Title Case'

export const process =
    input => 
        input.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1));