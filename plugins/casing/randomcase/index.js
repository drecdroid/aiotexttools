import Random from 'random-js'
import help from './help.md'

const random = new Random(Random.engines.browserCrypto)

export const help = help
export const title = 'Random Case'

export const process = 
    input =>
        Array.from(input).map(c => random.bool()? c.toLowerCase(): c.toUpperCase()).join('')
