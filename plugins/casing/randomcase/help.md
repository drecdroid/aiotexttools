# Randomcase
Changes everyletter casing to a random casing.

## Example

If input is:

```
HTML, CSS and Javascript
```

The output could be:

```
HtmL, CSs aND JAvascRiPt
```

or:

```
HTML, css and JAvAScRIpt
```

It's random after all.