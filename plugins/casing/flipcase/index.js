import { el, setChildren } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Flip Case'

export const process =
    input =>
        Array.from(input).map( c => c === c.toLowerCase() ? c.toUpperCase() : c.toLowerCase()).join('')