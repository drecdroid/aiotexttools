# Lowercase All

Lowercases every character.

## Example

If input is:
```
HTML, CSS and Javascript
```

The output will be:
```
html, css and javascript
```