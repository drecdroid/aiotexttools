import help from './help.md'

export const help = help
export const title = 'Lowercase All'

export const process =
    input => input.toLowerCase()
    