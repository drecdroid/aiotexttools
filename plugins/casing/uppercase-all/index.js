import help from './help.md'

export const help = help
export const title = 'Uppercase All'

export const process = 
    input_text =>
        input_text.toUpperCase()
    
