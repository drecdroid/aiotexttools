let sha1 = require('crypto-js/sha1')
import help from './help.md'

export const help = help
export const title = 'SHA1'

export const process =
    input =>
        sha1(input)

