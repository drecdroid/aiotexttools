let sha224 = require('crypto-js/sha224')
import help from './help.md'

export const help = help
export const title = 'SHA224'

export const process = 
    input =>
        sha224(input)

