let sha256 = require('crypto-js/sha256')
import help from './help.md'

export const help = help
export const title = 'SHA256'
export const process =
    input =>
        sha256(input)

