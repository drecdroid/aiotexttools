let sha3 = require('crypto-js/sha3')
import help from './help.md'

export const help = help
export const title = 'SHA3'

export const process =
    input => 
        sha3(input_text)