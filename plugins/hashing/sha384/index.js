let sha384 = require('crypto-js/sha384')
import help from './help.md'

export const help = help
export const title = 'SHA384'
export const process =
    input =>
        sha384(input)

