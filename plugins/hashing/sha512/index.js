let sha512 = require('crypto-js/sha512')
import help from './help.md'

export const help = help
export const title = 'SHA512'
export const process =
    input =>
        sha512(input)

