import help from './help.md'

export const help = help
export const title = 'Escape HTML'

export const process =
    input =>
        input.replace(/[<>"&]/g, m =>
            m === '<' ?
                '&lt;'
            : m === '>' ?
                '&gt;'
            : m === '&' ?
                '&amp;'
            : m === '"' ?
                '&quot;'
            : m
        )
