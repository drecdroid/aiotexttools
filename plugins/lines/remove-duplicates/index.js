import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Remove Duplicates'
export const process =
    input =>
        Array.from(new Set(input_text.split('\n'))).join('\n')

