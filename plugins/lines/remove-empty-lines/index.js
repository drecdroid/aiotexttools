import help from './help.md'

export const help = help
export const title = 'Remove Empty Lines'

export const process =
    input =>
        input.split('\n').filter(line=>line.trim()).join('\n')
