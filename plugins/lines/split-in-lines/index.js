import help from './help.md'

export const help = help
export const title = 'Split In Lines'
export const controls =
    settings => 
        el('label',
            'Split by: ',
            el('input', {type:'text', value: settings.separator, oninput: event => {
                    settings.separator = event.target.value
                }
            })
        )
            
export const defaultSettings =
    _ => ({
        separator : ','
    })

export const process =
    (input, settings) =>
        input.split(settings.separator).join('\n')    
