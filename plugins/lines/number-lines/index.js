import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Number Lines'
export const controls =
    settings =>
        [
            el('label', 'Number empty lines',
                el('input', { type : 'checkbox', checked : settings.empty_lines, onchange : event => {
                        settings.empty_lines = event.target.checked
                    }
                })
            ),
            el('label', 'Start at number',
                el('input', { type : 'number', value: settings.start_number, oninput : event => {
                        settings.start_number = event.target.value
                    }
                })
            ),
            el('label', 'Number suffix',
                el('input', { type : 'text', value: settings.number_suffix, oninput : event => {
                        settings.number_suffix = event.target.value
                    }
                })
            )
        ]

export const defaultSettings =
    _ => ({
        empty_lines : false,
        start_number : 1,
        number_suffix : '.-'
    })

export const process =
    (input, settings) => {
        let lineNumber = settings.start_number

        return input
        .split('\n')
        .map(line => {
            if(!line.trim() && !settings.empty_lines){
                return line
            }
            
            return `${lineNumber++}${settings.number_suffix}${line}`
        })
        .join('\n')
    }
        
