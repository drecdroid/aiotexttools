import help from './help.md'

export const help = help
export const title = 'Remove Line Breaks'
export const process =
    input => 
        input.split('\n').join('')
