import help from './help.md'

export const help = help
export const title = 'Sort Lines Alphabetically'
export const process =
    input => 
        input.split('\n').sort((l1, l2)=>l1.trim()>l2.trim()).join('\n')