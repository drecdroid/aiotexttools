import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Join Lines'
export const controls =
    settings =>
        el('label', 'Use as separator: ',
            el('input', {type:"text", value: settings.separator, oninput: event => {
                    settings.separator = event.target.value
                }
            })
        )

export const defaultSettings =
    _ => ({
        separator : ','
    })
    
export const process =
    (input, settings) =>
        input.split('\n').join(settings.separator)