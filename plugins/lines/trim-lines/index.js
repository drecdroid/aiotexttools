import help from './help.md'

export const title = 'Trim Lines'
export const help = help
export const process =
    input =>
        input.split('\n').map(l=>l.trim()).join('\n')


