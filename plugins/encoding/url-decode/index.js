import help from './help.md'

export const help = help
export const hidden = true
export const title = 'URL Decode'
export const process = 
    input => 
        input.replace(/(.\S+)/mg, decodeURI)
