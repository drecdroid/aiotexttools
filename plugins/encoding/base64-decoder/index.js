import help from './help.md'

export const help = help
export const title = 'Base64 Decoder'
export const process =
    input =>
        atob(input)
