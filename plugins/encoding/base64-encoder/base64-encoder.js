import help from './help.md'
export const help = help
export const title = 'Base64 Encoder'
export const process =
    input => 
        btoa(input)