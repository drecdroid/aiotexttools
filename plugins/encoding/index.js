import operations from './*/index.js'

export default {
    title : 'Encoding / Decoding',
    operations
}