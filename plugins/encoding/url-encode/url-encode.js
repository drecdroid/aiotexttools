import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'URL Encode'

export const controls =
    settings => 
        [
            el('label',
                el('span', 'Encode each line: '),
                el('input', {type:'checkbox', checked: settings.each_line, onchange: event => {
                        settings.each_line = !settings.each_line
                    }
                })
            )
        ]

export const defaultSettings =
    _ => ({
        each_line : true
    })

export const process =
    (input, settings) =>
        settings.each_line ?
            input.replace(/(.+)$/mg, encodeURI) :
            encodeURI(input)
