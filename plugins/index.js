import groups from './*/index.js'
import uuid from 'uuid'

export default {
    groups : groups.map(group => Object.assign(
        group.default,
        { id : group.default.title }
    )),
    operations : [].concat(
        ...groups.map(group => group.default.operations.map(
            operation => Object.assign(
                operation.default,
                operation,
                { 
                    id : uuid(),
                    group : group.default.title }
            )
        ))
    )
}