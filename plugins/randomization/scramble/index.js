import Random from 'random-js'
import Radio from 'libs/components/Radio'
import { el } from 'redom'
import help from './help.md'

let random = new Random(Random.engines.browserCrypto)

export const help = help
export const title = 'Scramble Text'
export const controls =
    settings =>
        new Radio('scramble',[
            {
                text : 'Everything',
                value : 'everything',
                checked : true
            },
            {
                text : 'Everythin Preserving Line Break',
                value : 'everything_break'
            },
            {
                text : 'Each Line',
                value : 'line'
            },
            {
                text : 'Each Word',
                value : 'word'
            },
            {
                text : 'Words in Lines',
                value : 'words_in_lines'
            }
        ], value => {
            settings.options
        })

function shuffle(str){
    return random.shuffle(Array.from(str)).join('')
}

export const process =
    (input, settings) => {
            switch(settings.options){
                case undefined:
                case 'everything':
                    return shuffle(input)
                case 'everything_break':
                    let shuffled = random.shuffle(Array.prototype.filter.call(input, c => c !== '\n'))
                    
                    return input
                            .split('\n')
                            .map(l=>l.length)
                            .map(b =>
                                shuffled
                                .splice(0, b)
                                .join('')
                            )
                            .join('\n')
                case 'line':
                    return input.replace(/.+/g, shuffle)
                case 'word':
                    return input.replace(/\w\S*/g, shuffle)
                /* case 'words_in_lines':
                    return input. */
            }
        }

