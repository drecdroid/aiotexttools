import Random from 'random-js'
import help from './help.md'

let random = new Random(Random.engines.browserCrypto)

export const help = help
export const title = 'Pick Random Line'
export const process = 
    input =>
        random.pick(input.split('\n'))
    