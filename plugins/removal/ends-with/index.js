import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Remove Lines Ending With'
export const controls =
    el('label',
        el('span', 'Needle:'),
        this.needle = el('input', {type: 'text'})
    )

export const process =
    (input, settings)=>
        settings.needle ?
            input
        : input.split('\n')
            .filter(l=>!l.endsWith(settings.needle))
            .join('\n')
