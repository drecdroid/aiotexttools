import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Remove Last n Characters'

export const controls = 
    settings =>        
            el('label',
                el('span', 'n:'),
                this.n = el('input', {type: 'number', min:0, value:0})
            )

export const process =
    (input, settings) =>
        input.replace(new RegExp(`.{0,${settings.n >=0 ? settings.n : 0}}$`, 'gm'),'')
