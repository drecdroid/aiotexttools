import { el } from 'redom'
import help from './help.md'

export const help = help

export const title = 'Remove Lines Starting With'
export const controls = 
    settings =>    
            el('label',
                el('span', 'Needle:'),
                this.needle = el('input', {type: 'text', value: settings.needle})
            )

export const process =
    (input, settings) =>
        !settings.needle ?
            input
        : input
            .split('\n')
            .filter(l=>!l.startsWith(settings.needle))
            .join('\n')
