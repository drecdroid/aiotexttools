import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Remove First n Characters'
export const controls = 
    settings =>
        el('label',
            el('span', 'n:'),
            el('input', {type: 'number', min:0, value: settings.n, oninput: event => {
                settings.n = event.target.value
            }})
        )

export const process =
    (input, settings) =>
        input.replace(new RegExp(`^.{0,${
            settings.n >= 0 ? settings.n : 0
        }}`, 'gm'),'')
