import { el } from 'redom'
const enc = require('crypto-js').enc
import help from './help.md'

const encodings = {
    Utf16 : 'Utf16',
    Utf16LE : 'Utf16LE',
    Utf8 : 'Utf8',
    Latin1 : 'Latin1',
}

export const help = help
export const title = 'Hexadecimal Representation'
export const controls =
    settings =>
        el('label', 'Encoding',
            el('select',
                el('option', {value:'Utf16', selected:true}, 'Utf16'),
                el('option', {value:'Utf16LE'}, 'Utf16LE'),
                el('option', {value:'Utf8'}, 'Utf8'),
                el('option', {value:'Latin1'}, 'Latin1')
            )
        )

export const defaultSettings =
    _ => ({
        encoding : encodings.Utf16
    })

export const process =
    (input, settings)=>
        enc[settings.encoding].parse(input).toString().match(/.{2}/g).join(' ')

