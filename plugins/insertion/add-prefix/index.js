import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Add Prefix'

export const controls =
    settings =>
        el('label', 'Prefix: ',
            el('input', {type:'text', value:settings.prefix, oninput: event => {
                    settings.prefix = event.target.value
                }
            })
        )

export const defaultSettings =
    _ => ({
        prefix : ''
    })

export const process =
    (input, settings) =>
        input.split('\n').map(l=> l.trim()? `${settings.prefix}${l}`: l).join('\n')