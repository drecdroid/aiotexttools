import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Add Suffix'

export const controls =
    settings =>
        el('label', 'Suffix: ',
            el('input', {type:'text', value:settings.suffix, oninput: event => {
                    settings.suffix = event.target.value
                }
            })
        )

export const defaultSettings =
    _ => ({
        suffix : ''
    })

export const process =
    (input, settings) =>
        input.split('\n').map(l=> l.trim()? `${l}${settings.suffix}`: l).join('\n')