# AIO TEXT TOOLS

## How to use

First add some text operations from the text operations panel. We will add the `split in lines` operation in this example

![example image](img/index-text-operations.png "An exemplary image")

Now you will have an operation widget in the operations pipeline.

![example image](img/index-operation-widget.png "An exemplary image")

Modify its settings as desired. In this example we will break the lines by the `:` character

![example image](img/index-split-in-lines-settings.png "An exemplary image")

## Widget Controls

- You can collapse/expand a widget by clicking on the collapse icon or on its name.
- Click the eye icon to bypass the widget during the pipeline execution.
- Click the information icon to get information about how to use the widget.
- Reorder the widget pipeline by dragging(on desktop) or clicking the up or down arrow icons(mobile or desktop).
- Delete the widget by clicking the trash icon.

## Request Your Text Operations

If you need some text operations write me to **admin@tulz.io** or send me a PM to **/u/drecdroid**

## Help Us

We don't ask money as retribution but you can helps us by:

- Using this tool and enjoying it.
- Sharing this tool with your friends.

## TODO

### Basic
- Column extractor
- Remove Lines Containing

### Randomization

- Randomize string

### Encoding / Decoding
- Encode/Decode URL

### Escaping
- Un/Escape XML
- Un/Escape SQL

### Others

- Tabs to spaces and viceversa
- Remove punctuation
- Remove all whitespace
- Disenvowell

## Upcoming Features

- Save pipeline
- Input widget(generator, file, url response, etc.)

## FAQ

### What did you use to make this?

#### Dependencies
- re:dom
- crypto-js
- random-js

#### Dev-dependencies
- browserify
- watchify
- require-globify
- marked : For html help files generation
- node-sass

### Why don't you use React?
It's easier to use re:dom

### Why don't you use webpack?
It's easier to use browserify

### Did you use typescript?
No

### Why?
Because this project is not so big to require using it.

### Did you transpile your ES6?
No

### Why?
I think we are on times to move forward, we should not retain everyone in the past because we don't want to upgrade our browsers.
Also I'm lazy.