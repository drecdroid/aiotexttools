const { el } = require('redom')
import help from './help.md'

export const help = help
export const title = 'Set Scheme'

export const controls =
    settings =>
        el('label', 'New protocol',
          el('input', { type : 'text', value: settings.protocol, oninput: event =>{
                    settings.protocol = event.target.value
                }
            })
        )

export const process =
    (input, settings) =>
        input.split('\n').map(line =>{
            settings.protocol + (line.split('://')[1] || "")
        }).join('\n')
