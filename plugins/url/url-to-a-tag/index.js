import urlRegex from 'libs/regex/url'
import help from './help.md'

export const help = help
export const title = 'URL to A tags'

export const process = 
    input =>
        input.replace(new RegExp(urlRegex, 'gi'), '<a href="$&">$&</a>')
