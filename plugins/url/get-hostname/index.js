import help from './help.md'

export const help = help
export const title = 'Get Hostname'
export const process =
    input =>
        input.split('\n').map(line =>
            line.replace(/(.+):\/\//, "")
                .split(/[/?:@]/)[0]
        ).join('\n')
