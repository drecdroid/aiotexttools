import urlRegex from 'libs/regex/url'
import help from './help.md'

export const help = help
export const title = "Extract URLs"

export const process =
    input =>
        input.match(new RegExp(urlRegex, 'gi')).join('\n')