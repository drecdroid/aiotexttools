import { el } from 'redom'

const Radio = require('libs/components/Radio')

let AES = require('crypto-js').AES
let Utf8 = require('crypto-js').enc.Utf8

export const title = 'AES'

export const controls =
    settings =>
        [
            el('form',
                el('label', 'Secret key: ',
                    el('input', {type:"text", value: settings.secret_key, oninput : event =>{
                            settings.secret_key = event.target.value
                        }
                    })
                ),
                new Radio('operation',
                    [
                        {
                            text: 'encrypt',
                            value : 'encrypt',
                            checked : true
                        },
                        {
                            text: 'decrypt',
                            value : 'decrypt'
                        }
                    ],
                    value => {
                        settings.operation = value
                    }
                )
            )
        ]

export const process =
    (input, settings) =>
        settings.operation === 'encrypt' ?
            AES.encrypt(input, settings.secret_key).toString()
        : settings.operation === 'decrypt' ?
            AES.decrypt(input, settings.secret_key).toString(Utf8)
        : input