# Find & Replace

Example:
We want to put parentheses around any multiplication or division
```
    input -> 1 + 2 * 41 - 10 / 5 = x / (10 + 10)

    pattern : (\w+\s*[*/]\s*\w+|\w+\s*[*/]\s*\(.+\)|\(.+\)\s*[*/]\s*\w+)
    replacement : ($1)

    output -> 1 + (2 *41) - (10 / 5) =(x / (10 + 10))
```