import { el } from 'redom'
import help from './help.md'

const escapeRegex = str =>
    str.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')

export const help = help
export const title = 'Find & Replace'

export const controls =
    settings =>
        [
            el('label',
                el('input', {type:'checkbox', value: settings.global, onchange: event => {
                        settings.global = !settings.global
                    }
                }),
                el('span', 'Global')
            ),
            el('br'),
            el('label',
                el('input', {type:'checkbox'}),
                el('span', 'Multiline')
            ),
            el('br'),
            el('label',
                el('input', {type:'checkbox'}),
                el('span', 'Case Insensitive')
            ),
            el('br'),
            el('label',
                el('span', 'Use Regex'),
                el('input', {type:'checkbox'})
            ),
            el('br'),
            el('label',
                el('span', 'Pattern'),
                el('input', {type:'text'})
            ),
            el('br'),
            el('label', 'Replace With',
                el('input', {type:'text'})
            )
        ]

export const process =
    (input, settings) =>
        input.replace(
            new RegExp(
                settings.use_regex ?
                    settings.pattern :
                    escapeRegex(JSON.parse(`"${settings.pattern}"`))
                ,
                [
                    settings.multiline ? 'm':'',
                    settings.global ? 'g':'',
                    settings.insensitive ? 'i':'',
                ].join('')
            ),
            JSON.parse(`"${settings.replacement}"`)
        )

export const defaultSettings =
    () => ({
        pattern : '',
        use_regex : false,
        multiline : false,
        global : true,
        insensitive : false,
        replacement : '',
    })
