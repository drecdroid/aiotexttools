import { el } from 'redom'
import help from './help.md'

export const help = help
export const title = 'Replace Tabs with Spaces'

export const controls =
    settings =>
        el('label',
            el('span', 'Spaces: '),
            el('input', {type:'number', value: settings.spaces, min:0, oninput: event => {
                    settings.spaces = event.target.value
                }
            })
        )
    
export const defaultSettings =
    _ => ({
        spaces : 4
    })

export const process =
    (input, settings) =>
        input.replace(/\t/g, ' '.repeat(settings.spaces))
