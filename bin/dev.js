#!/usr/bin/env node

let cp = require('child_process')
const fse = require('fs-extra')
const fs = require('fs')

;[
    'dev-public/',
    'dev-public/js/',
    'dev-public/css/',
    'dev-public/img/',
].forEach(d => fs.existsSync(d) || fs.mkdirSync(d))
console.log('dirs')

fse.copySync('index.html', 'dev-public/index.html')
fse.copySync('index', 'dev-public/')
fse.copySync('img', 'dev-public/img', {
    overwrite: true,
    recursive: true,
    preserveTimestamps: true,
})

;[
    'serve dev-public',
    'node-sass -w --source-map true --source-map-contents true scss/style.scss -o dev-public/css/',
    'watchify -t [ require-globify ] -t [ babelify --presets [ es2015 ] ] --debug src/index.js -o dev-public/js/main.js -v',
    'node bin/build-help.js -d',
    'node bin/sw.js -d'
].forEach(cmd => {
    let parts = cmd.split(' ')
    cp.spawn(
        parts[0],
        parts.slice(1), 
        { env : process.env, shell : true, stdio: 'inherit'}
    )
})