#!/usr/bin/env node

const md = require('marked')
const fs = require('fs')
const path = require('path')

let dev = process.argv[2] === '-d'

let src = 'src/plugins/'
let out = `${dev?'dev-public':'public'}/help/`
fs.existsSync(out) || fs.mkdirSync(out)

function renderMarkdown(filename){
    fs.readFile(filename, 'utf-8', (err, data) => {
        let md_out = md(data)
        let dirname = path.join(out, path.dirname(path.relative(src, filename)))

        if(!fs.existsSync(dirname)){
            fs.mkdirSync(dirname)
        }

        let path_out = path.join(dirname, path.basename(filename, '.md') + '.html')
        console.log(`writing file "${path_out}"`)
        fs.writeFile(path_out, md_out, (err)=>{
            if (err) throw err;
            console.log('The file has been saved!');
        })
    })
}

function renderFile(filename){
    if(filename.endsWith('md')){
        renderMarkdown(filename)
        if(process.argv[3] === '-d'){
            fs.watch(filename, (event, file)=>{
                renderMarkdown(filename)
            })
        }
    }
}

function buildDir(dir){
    fs.readdir(dir, (err, files)=>{
        files.forEach(
            file => {
                let filename = path.join(dir, file)
                fs.lstat(filename, (err, stats) => {
                    if(stats.isDirectory()){
                        buildDir(filename)
                    }else{
                        renderFile(filename)
                    }
                })
            }    
        )
    })
}

buildDir(src)