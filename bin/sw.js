const sw = require('sw-precache')

let dev = process.argv[2] === '-d'

let out = `${dev?'dev-public':'public'}/`

sw.write(`${out}service-worker.js`, {
    staticFileGlobs: [
        `${out}css/**.css`,
        `${out}help/index.html`,
        `${out}index.html`,
        `${out}js/**.js`,
    ],
    stripPrefix: `${out}`,
    runtimeCaching: [{
        urlPattern: /(img|help)\/.+/,
        handler: 'networkFirst'
    },{
        urlPattern : /https\:\/\/fonts\.(googleapis|gstatic)\.com\/.+/,
        handler : 'networkFirst'
    },{
        urlPattern : /https\:\/\/maxcdn.bootstrapcdn.com\/.+/,
        handler : 'networkFirst'
    }]
})