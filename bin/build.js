#!/usr/bin/env node

let cp = require('child_process')
const fse = require('fs-extra')
const fs = require('fs')

;[
    'public/',
    'public/js/',
    'public/css/',
    'public/img/',
].forEach(d => fs.existsSync(d) || fs.mkdirSync(d))

fse.copySync('index.html', 'public/index.html')
fse.copySync('index', 'public/')
fse.copySync('img', 'public/img', {
    overwrite: true,
    recursive: true,
    preserveTimestamps: true,
})

cp.execSync('npm install')

;[
    'node-sass scss/style.scss -o public/css/',
    'browserify -t [ require-globify ] -t [ babelify --presets [ es2015 ] ] src/index.js -o public/js/main.js -v',
    'node bin/build-help.js',
    'node bin/sw.js',    
].forEach(cmd => {
    let parts = cmd.split(' ')
    cp.spawn(
        parts[0],
        parts.slice(1), 
        { env : process.env, shell : true, stdio: 'inherit'}
    )
})