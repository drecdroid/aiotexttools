const { el, mount, list } = require('redom')
import { move_down, move_up, view_info } from './actions'
module.exports = class {

    constructor({ app }, data, index, collection){
        
        this.title = this.constructor.title || ""
        this.data = data

        this.el = el(
            '.text-operation.expanded',
            el('', {
                draggable : true,
                className : 'text-operation-dragarea',
                ondragstart : (e) => this.handleDrag(e),
                ondragend : (e) => this.handleDragEnd(e),
                ondragenter: (e) => this.handleDragEnter(e)
            },
                el('i', {
                    className : 'expanded-icon fa',
                    onclick : (e) => this.toggleExpanded(e)
                }),
                el('label.enabled-icon.fa.fa-eye',
                    this.enabledControl = el('input', {type:"checkbox", checked: true, hidden: true})
                ),
                this.titleControl = el('span.text-operation-title', this.title, {
                    onclick : (e) => this.toggleExpanded(e)
                }),
                el('i.info-icon.fa.fa-info', {
                    onclick : (event) => {
                        app.do(view_info(data.help))
                    }
                }),
                el('i.move-up-icon.fa.fa-arrow-up',{
                    onclick : (event) => {
                        app.do(move_up(data.id))
                    }
                }),
                el('i.move-down-icon.fa.fa-arrow-down',{
                    onclick : (event) => {
                        app.do(move_down(data.id))
                    }
                }),
                el('i.delete-icon.fa.fa-trash', {
                    onclick : (event) => {
                        app.do({
                            action : 'delete_operation',
                            id : data.id
                        })
                    }
                })
            ),
            el('.text-operation-controls',
                data.controls
            ),
            el('.drop-zone.drop-over',
            {
                ondragover : (e) => this.handleDragOver(e),
                ondrop : (e) => this.handleDrop(e)
            }),
            el('.drop-zone.drop-below',{
                ondragover : (e) => this.handleDragOver(e),
                ondrop : (e) => this.handleDrop(e)
            })
        )

        this.infoControl.addEventListener('click', e => {
            this.el.dispatchEvent(new CustomEvent('view-info', {
                detail: this.data,
                bubbles: true
            }))
        })

        this.enabledControl.addEventListener('change', e => {
            this.enabled = this.enabled
        })

        this.createControls()
    }


    set enabled(value){
        this.el.classList.toggle('disabled', !value)
    }

    get enabled(){
        return this.enabledControl.checked
    }

    update(data, index, collection){
        this.data = data
    }
    
    handleDrag(e){
        e.dataTransfer.setData('text/plain', this.data.id)
        e.dropEffect = "move"
    }

    handleDragEnter(e){
        document.body.classList.toggle('dragging', true)
    }

    handleDragEnd(e){
        document.body.classList.toggle('dragging', false)
    }

    handleDragOver(e){
        e.preventDefault()
        e.dataTransfer.dropEffect = "move"
    }

    handleDrop(e){
        let origin_id = e.dataTransfer.getData('text')
        if(this.data.id !== origin_id){
            if(e.target.classList.contains('drop-over')){
                this.moveOver(origin_id, this.data.id)
            }else{
                this.moveBelow(origin_id, this.data.id)
            }
        }
    }

    toggleExpanded(e){
        this.el.classList.toggle('expanded')        
    }

    process(input_text){
        return input_text
    }
}
