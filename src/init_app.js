import plugins from 'plugins'

export default function init_app() {
    // add events to bar

    const app = {}
    return app
}

const create_operation = operation => {
    const op_module = plugins.operation(operation)

    const settings = op_module.defaultSettings ?
        op_module.defaultSettings() : {}

    return {
        help : op_module.help || '',
        controls : op_module.controls(settings),
        process : input => op_module(input, settings),
        enabled : true,
    }
}

function init_store(store, proposal){
    return {
        operations : [],
        input: '',
        output : ''
    }
}

export const with_action = (action_name, mutator) =>
    (store, proposal) =>
        proposal.action === action_name ?
            mutator(store, proposal)
            : {}

export const add_operation = with_action('add_operation', (store, proposal) => ({
        ...store,
        operations: [...store.operations, {
            ...create_operation(proposal.operation_type),
            id: uuid()
        }]
    })
)

export const pipe_mutators = (...mutators) =>
    (store, proposal) =>
        mutators.reduce((store, mutator) => ({ ...store, ...mutator(store, proposal) }) , store)

const delete_operation = (store, proposal) => ({
        ...store,
        operations : store.operations.filter(_=>_.id !== proposal.id)
    })

const diff_operations = store =>
    store.operations

app.on(diff_operations, (new_state, prev_state)=>{

}, [])

function prepare_dom(app){

    app.on('open_sidear', ({}) => {

    })

    let viewDefaultInfo = document.getElementById('info-button')

    let ontouch = (e) => {
        if(e.target.classList.contains('menu-toggle')){
            app.do('view_menu')
            //document.body.classList.toggle('controls_sidebar_opened')
            //document.body.classList.toggle('info_sidebar_opened', false)
        }
        else
        if(e.target.classList.contains('info-toggle')){
            app.do('view_info')
            //document.body.classList.toggle('controls_sidebar_opened',false)
            //document.body.classList.toggle('info_sidebar_opened')
        }

        if(window.innerWidth <= 640){
            if(document.body.classList.contains('controls_sidebar_opened')){
                if(e.clientX > this.controls_sidebar.clientWidth){
                    document.body.classList.toggle('controls_sidebar_opened', false)
                }
            }

            if(document.body.classList.contains('info_sidebar_opened')){
                if(e.clientX < this.info_sidebar.offsetLeft){
                    document.body.classList.toggle('info_sidebar_opened', false)
                }
            }
        }
        e.stopPropagation();
        return false;
    }

    if(document.ontouchstart){
        document.addEventListener('touchstart', ontouch)
    }else{
        document.addEventListener('click', ontouch)
    }


    document.addEventListener('view-info', e=>{
        setTimeout(
            ()=>{
                document.body.classList.toggle('info_sidebar_opened', true)
                if(window.innerWidth <= 900){
                    document.body.classList.toggle('controls_sidebar_opened',false)
                }
            },
            30
        )
    })
}