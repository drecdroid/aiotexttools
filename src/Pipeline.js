const { el } = require('redom')
const OperationsList = require('./OperationsList')

const set_input = value => ({
    action : 'set_input',
    value
})

module.exports = class {
    constructor(app){
        this.app = app
        this.el = el('',
            el('.workspace',
                el('textarea.text-input', { onchange: (event)=>{
                        app.do(
                            set_input(event.target.value)
                        )
                    }
                }),
                new OperationsList(app),
                el('button.process-btn.control', 'Run Text Operations', { onclick: ()=>this.run_pipeline() }),
                this.outputText = el('textarea.text-output'),
                el('.output-buttons',
                    el('button.control.control-bar', 'Copy Output', { onclick : ()=>this.copy() }),
                    el('button.control.control-bar', 'Download Output', { onclick : ()=>this.download() }),
                    el('button.control.control-bar', 'Use Ouput As Input', {onclick : ()=>this.reinput() })
                )
            )
        )

        app.on('change_output', ({output})=> {
            this.outputText.value = output
        })
    }

    copy(){
        this.app.do('copy_output')
    }

    download(){
        this.app.do('download_output')
    }
    
    reinput(){
        this.app.do(
            set_input(this.outputText.value)
        )
    }

    run_pipeline(){
        this.app.do('run_pipeline')
    }
}