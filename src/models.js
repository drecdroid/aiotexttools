const plugins = require('plugins')
import uuid from 'uuid'

const create_operation = operation => {
    const op_module = plugins.operation(operation)

    const settings = op_module.defaultSettings ?
        op_module.defaultSettings() : {}

    return {
        help : op_module.help || '',
        controls : op_module.controls(settings),
        process : input => op_module(input, settings)
    }
}

const add_operation = (operation, operations) => {
    operation.id = operation.id || uuid()
    operations.push(operation)
}

const moveBelow = (origin_id, target_id) => {
    let o_index = this.operations.findIndex(v=>v.id === origin_id)
    let t_index = this.operations.findIndex(v=>v.id === target_id)
    
    this.operations = insertAfter(this.operations, o_index, t_index)
    this.update(this.operations)
}

const moveOver = (origin_id, target_id) => {
    let o_index = this.operations.findIndex(v=>v.id === origin_id)
    let t_index = this.operations.findIndex(v=>v.id === target_id)
    this.operations = insertBefore(this.operations, o_index, t_index)
    this.update(this.operations)
}

const moveUp = (operation_id) => {
    let id = this.operations.findIndex(v=>v.id === operation_id)
    this.operations = insertBefore(this.operations, id, id-1)
    this.update(this.operations)
}

const moveDown = (operation_id) => {
    let id = this.operations.findIndex(v=>v.id === operation_id)
    this.update(
        this.operations = insertAfter(this.operations, id, id+1)
    )
}

const deleteOperation = (operation_id) => {
    this.operations = this.operations.filter(operation=> operation.id !== operation_id)
    this.update(this.operations)
}

function insertAfter(arr, origin, target){
    if(target>arr.length-1) return arr

    if(origin > target){
        arr = [...arr.slice(0,target+1), arr[origin], ...arr.slice(target+1, origin), ...arr.slice(origin+1)]
    }
    else{
        arr = [...arr.slice(0,origin), ...arr.slice(origin+1, target+1), arr[origin], ...arr.slice(target+1)]
    }
    return arr
}

function insertBefore(arr, origin, target){
    if(target<0) return arr

    if(origin > target){
        arr = [...arr.slice(0,target), arr[origin], ...arr.slice(target, origin), ...arr.slice(origin+1)]
    }else{
        arr = [...arr.slice(0,origin), ...arr.slice(origin+1, target), arr[origin], ...arr.slice(target)]
    }
    return arr
}


function copy_output(){
    this.outputText.select()
    document.execCommand('copy')
    this.outputText.selectionEnd = this.outputText.selectionStart
    this.outputText.blur()
}

function download_output(){
    let link = (el =>(
        el = document.createElement('a'),
        el.download = 'output.txt',
        el.href = `data:text/plain;charset=UTF-8,${encodeURIComponent(this.outputText.value)}`,
        el)
    )()

    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
}










