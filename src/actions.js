
export const move_up = id => ({
        action : 'move_up',
        id
    })

export const move_down = id => ({
        action : 'move_down',
        id
    })

export const view_info = src => ({
    action : 'view_info',
    src
})

export const delete_operation = id => ({
    action : 'delete_operation',
    id
})