let { el, mount, list } = require('redom')

module.exports = class {
    constructor(left, middle, right){

        this.viewDefaultInfo.addEventListener('click', e => {
            this.el.dispatchEvent(new CustomEvent('view-info', { detail:'default', bubbles: true }))
        })

        let ontouch = (e) => {
            if(e.target.classList.contains('menu-toggle')){
                document.body.classList.toggle('controls_sidebar_opened')
                document.body.classList.toggle('info_sidebar_opened', false)
            }
            else
            if(e.target.classList.contains('info-toggle')){
                document.body.classList.toggle('controls_sidebar_opened',false)
                document.body.classList.toggle('info_sidebar_opened')
            }

            if(window.innerWidth <= 640){
                if(document.body.classList.contains('controls_sidebar_opened')){
                    if(e.clientX > this.controls_sidebar.clientWidth){
                        document.body.classList.toggle('controls_sidebar_opened', false)
                    }
                }

                if(document.body.classList.contains('info_sidebar_opened')){
                    if(e.clientX < this.info_sidebar.offsetLeft){
                        document.body.classList.toggle('info_sidebar_opened', false)
                    }
                }
            }
            e.stopPropagation();
            return false;
        }

        if(document.ontouchstart){
            document.addEventListener('touchstart', ontouch)
        }else{
            document.addEventListener('click', ontouch)
        }


        document.addEventListener('view-info', e=>{
            setTimeout(
                ()=>{
                    document.body.classList.toggle('info_sidebar_opened', true)
                    if(window.innerWidth <= 900){
                        document.body.classList.toggle('controls_sidebar_opened',false)
                    }
                },
                30
            )
        })
    }
}