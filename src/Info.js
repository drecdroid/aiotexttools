const { el } = require('redom')

module.exports = class {
    constructor(app){
        this.el = el('iframe.tool-info', { src: 'info.html' })
        
        app.on('view_info', ({ src }) => {
            this.set_src(src)
        })
    }

    set_src = (src) => {
        this.el.src = src || 'info.html'
    }
}