const { el, list } = require('redom')
const uuid = require('uuid')


module.exports = class {
    constructor(app){
        this.el = el('.text-operation-list')
        this.list = list(this.el, TextOperationFactory, 'id', {
            app
        })

        app.on('operations_modified', ({ operations })=>{
            this.update(operations)
        })
    }

    process(input_text){
        return this.list.views.filter(view=>view.enabled).reduce( (pv, cv) => cv.process(pv), input_text)
    }

    update(data){
        this.list.update(data)
    }
}