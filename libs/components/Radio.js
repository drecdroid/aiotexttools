const { el, setChildren } = require('redom')

module.exports = class {
    constructor(name, options){
        this.el = el('form',{
                onsubmit : e=>e.preventDefault()
            },
            options.map(option => el(
                'label', 
                el('input', {name, type:'radio', value: option.value, checked: option.checked || false }),
                el('span', option.text)
            ))
        )
        
        this.el.addEventListener('click', e => {
            if(e.target.getAttribute('name') === name){
                this.value = e.target.value
            }
        })
    }
}
