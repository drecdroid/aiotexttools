const path = require('path')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const ScriptPreloader = require('preload-webpack-plugin')
const StylePreloader = require('preload-webpack-plugin')
const CriticalPlugin = require('webpack-plugin-critical').CriticalPlugin;
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry : './src/index.js',
    output: {
        filename : 'script.js',
        path: path.resolve(__dirname, 'webpack-public')
    },
    resolve: {
        modules : [
            __dirname,
            'node_modules'
        ]
    },
    module : {
        rules : [
            {
                test : /\.js$/,
                enforce : 'pre',
                loader: 'import-glob',
            },
            {
                test : /\.scss$/,
                use : ExtractTextPlugin.extract({
                    fallback : 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            }   ,
            {
                test: /\.md$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].html'
                        }  
                    },
    
                    {
                        loader: "markdown-loader"
                    }
                ]
            }
        ]
    },
    plugins : [
        new HTMLWebpackPlugin({
            template : 'src/template.ejs'
        })/* ,
        new FaviconsWebpackPlugin({
            logo: './assets/favicon/favicon.png',
            inject: true,
            background: '#404040',
            icons: {
                android: true,
                appleIcon: true,
                appleStartup: true,
                coast: false,
                favicons: true,
                firefox: true,
                opengraph: false,
                twitter: false,
                yandex: false,
                windows: false
            }
        }) */,
        new CleanWebpackPlugin(['webpack-public'] ),
        new ExtractTextPlugin('style.css'),
        new CriticalPlugin({
            src: 'index.html',
            inline: true,
            minify: true,
            dest: 'index.html'
        })
    ]
}